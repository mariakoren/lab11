import { MongoClient } from "mongodb";

const mongoUser = process.env.MONGO_INITDB_ROOT_USERNAME;
const mongoPassword = process.env.MONGO_INITDB_ROOT_PASSWORD;
const mongoHost = process.env.DB_HOST;

const url = `mongodb://${mongoUser}:${mongoPassword}@${mongoHost}:27017`;
const client = new MongoClient(url);

const getDb = () => client.db("database");

export default getDb;
